<?php 
    
require_once 'modelos/Feed_Modelo.php';
    class Feed_Controlador
    {

        
        public function __construct(){
            $this->modelo = new Feed_Modelo();
        }
        
        //Este metodo muestra la vista del listado, (pagina principal)
        public function index(){
            $listado_feeds= $this->modelo->obtener_todos();
            require_once 'vistas/header.php';
            require_once 'vistas/feed_listado_vista.php';
            require_once 'vistas/footer.php';
        }
        
        //Este metodo muestra el feed especifico
        public function obtener(){
            $feed = new Feed_Modelo();
            
            if(isset($_REQUEST['idFeed'])){
                $feed = $this->modelo->obtener($_REQUEST['idFeed']);
            }
            require_once 'vistas/header.php';
            require_once 'vistas/feed_detalle_vista.php';
            require_once 'vistas/footer.php';
        }
        
        //Este metodo eliminara el feed deseado
        public function eliminar(){
            $this->modelo->eliminar($_REQUEST['idFeed']);
            header('Location: index.php');
        }
        
        //Este metodo actualizara los atributos del feed
        public function actualizar(){

            $title= $_REQUEST['title'];
            $body= $_REQUEST['body'];
            $image= $this->crearRutaImagen();
            $source= $_REQUEST['source'];
            $publisher= $_REQUEST['publisher'];
            $idFeed= $_REQUEST['idFeed'];
            
            $this->modelo->actualizar($title, $body,$image, $source, $publisher, $idFeed);
            header('Location: index.php');
            
        }
        
        //Este metodo creara un feed nuevo
        public function nuevo(){
            $feed = new Feed_Modelo();
            require_once 'vistas/header.php';
            require_once 'vistas/feed_crear_vista.php';
            require_once 'vistas/footer.php';
        }
        
        //Este metodo guardara los atributos del nuevo feed
        public function guardar(){
            
            $title= $_REQUEST['title'];
            $body= $_REQUEST['body'];
            $image= $this->crearRutaImagen();
            $source= $_REQUEST['source'];
            $publisher= $_REQUEST['publisher'];

            $this->modelo->crear($title, $body,$image, $source, $publisher);
            header('Location: index.php');
        }
        //funcion que guarda una imagen en el servidor y devuelve su ruta, si algo va mal devuelve una ruta vacia
        private function crearRutaImagen(){
            
            if(is_uploaded_file($_FILES['fotoNoticia']['tmp_name'])===true){
                //si el tipo es jpeg lo convierte en jpg
                $tipo= $_FILES['fotoNoticia']['type'];
                $tipo= explode('/', $tipo)[1];
                if(strcmp($tipo,'jpeg')==0){
                    $tipo='jpg';
                    
                }
                //Comprueba que la imagen sea de uno de estos tres tipos
                if(strcmp($tipo, 'jpg')==0 || strcmp($tipo, 'gif')== 0 || strcmp($tipo, 'png')==0){
                    //Se coge el nombre del archivo que ya tenia
                    $nombre_archivo= $_FILES['fotoNoticia']['name'];
                    $nombre_archivo_sintipo= explode('.', $nombre_archivo)[0];
                    //Se genera la ruta 
                    $fichero_subido= 'imagenes/'. $nombre_archivo_sintipo.'.'.$tipo;
                    //Crea la carpeta imagenes si no existe
                    if(!is_dir('imagenes')){
                        mkdir('imagenes',0777, true);  
                    }
                    //Se mueve el archivo all servidor
                    move_uploaded_file($_FILES['fotoNoticia']['tmp_name'], $fichero_subido);
                    return $fichero_subido;

                }
                else{
                    return '';
                    echo 'Error al guardar el fichero';
                }
            }
            else{
                return '';
            }
        }
        
    }
?>