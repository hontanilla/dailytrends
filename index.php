<?php
require_once('bd/conexion.php');
require_once('controladores/Feed_Controlador.php');

//Analiza los parametros de la peticion HTTP comprobando si la accion solicitada existe en el controlador, si no, se muestra Index.
$controlador= new Feed_Controlador();

if(!empty($_REQUEST['a'])){
    $accion=$_REQUEST['a'];
    if (method_exists($controlador, $accion)) {
        $controlador->$accion();
    }else{
        $controlador->index();
    }
}else{
    $controlador->index();
}

?>