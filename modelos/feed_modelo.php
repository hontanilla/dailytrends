
<?php 

    include_once 'bd/conexion.php';
    
    class Feed_Modelo
    {
        private $_pdo;
        
        public $id;
        public $title;
        public $body;
        public $image;
        public $source;
        public $publisher;
        
        
        public function __construct()
        {
            try {
                $this->pdo = conexion::conectar();
            }
            catch(Exception $e)
            {
                die($e->getMessage());
            }
            
        }
        
        public function obtener($idFeed)
        {
            try
            {
                $mostrar_feed= $this->pdo->prepare('SELECT * FROM feed WHERE id= :idFeed;');
                $mostrar_feed->bindParam(':idFeed',$idFeed);
                $mostrar_feed->execute();
                $feed= $mostrar_feed->fetch(PDO::FETCH_OBJ);
                return $feed;
            }
            catch (Exception $e)
            {
                die($e->getMessage());
            }
        }
        
        public function obtener_todos()
        {
            try
            {
                $mostrar_feeds= $this->pdo->prepare('SELECT * FROM feed');
                $mostrar_feeds->execute();
                $feeds= $mostrar_feeds->fetchAll(PDO::FETCH_OBJ);
                return $feeds;
            }
            catch (Exception $e)
            {
                die($e->getMessage());
            }
        }
        
        public function eliminar($idFeed)
        {
            try 
            {
                $eliminar_feed= $this->pdo->prepare('DELETE FROM feed WHERE id= :idFeed;');
                $eliminar_feed->bindParam(':idFeed',$idFeed);
                $eliminar_feed->execute();
            }
            catch (Exception $e)
            {
                die($e->getMessage());
            }
        }
        
        public function actualizar($title,$body,$image,$source,$publisher,$id)
        {
            try
            {
                $actualizar_feed= $this->pdo->prepare('UPDATE feed SET title= :title, body= :body, image= :image, source= :source, publisher= :publisher WHERE id= :idFeed;');
                $actualizar_feed->bindParam(':title',$title);
                $actualizar_feed->bindParam(':body',$body);
                $actualizar_feed->bindParam(':image',$image);
                $actualizar_feed->bindParam(':source',$source);
                $actualizar_feed->bindParam(':publisher',$publisher);
                $actualizar_feed->bindParam(':idFeed',$id);
                $actualizar_feed->execute();
            }
            catch (Exception $e)
            {
                die($e->getMessage());
            }  
        }
        
        public function crear($title,$body,$image,$source,$publisher)
        {
            try 
            {
                $crear_feed= $this->pdo->prepare('INSERT INTO feed(title, body, image, source, publisher) VALUES(:title, :body, :image, :source, :publisher);');
                $crear_feed->bindParam(':title',$title);
                $crear_feed->bindParam(':body',$body);
                $crear_feed->bindParam(':image',$image);
                $crear_feed->bindParam(':source',$source);
                $crear_feed->bindParam(':publisher',$publisher);
                $crear_feed->execute();
            }
            catch (Exception $e)
            {
                die($e->getMessage());
            }
        }
    }
?>