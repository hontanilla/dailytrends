<?php
class conexion
{

    //dbname = dailytrends
    //dailytrends = nombre de usuario
    //'' = contraseña de dailytrends, no lleva contraseña.
    
    public static function conectar()
    {
        $pdo = new PDO('mysql:host=localhost;dbname=dailytrends;charset=utf8', 'dailytrends', '');
        //Filtrando posibles errores de conexión.
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $pdo;
    }
}
