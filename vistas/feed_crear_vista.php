
<div class="row">
	<section class="col-md-12">
		<h2 class="page-header col-xs-12">Crear Noticia</h2>
	</section>
</div>
<div class="row">
	<div class="col-md-6 col-xs-6">
		<a class="btn boton_header" href="index.php" role="button">Listado Noticias</a>
	</div>
</div>

<div class="row">
	<div class="col-md-12 col-xs-12">
		<form id="editar_noticia" name="editar_noticia" method="post" action="?a=guardar" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12 col-xs-12 datos_noticia">
					<div class="col-md-3">
						<label class="datos_noticia">Foto</label> 
						<input type="file" name="fotoNoticia" value="" />
					</div>
					<div class="col-md-3">
						<label class="datos_noticia">Title</label> 
						<input class="campos" type="text" name="title" value="" />
					</div>
					<div class="col-md-3">
						<label class="datos_noticia">Source</label> 
						<input class="campos" type="text" name="source" value="" />
					</div>
					<div class="col-md-3">
						<label class="datos_noticia">Publisher</label> 
						<input class="campos" type="text" name="publisher" value="" />
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 col-xs-12 campo">
					<label class="titulo_body">Body</label>
				</div>
			</div>
			<div class="row">
				<textarea name="body" class="descripcion" rows="8"></textarea>
				<br>
			</div>

			<div class="row">
				<div class="col-md-12 col-xs-12 botones">
					<input type="hidden" name= "MAX_FILE_SIZE" value="10000000">
					<input type="hidden" name="xxx" value="1">
					<input type="submit" class="btn btn-info botones" name="guardar" role="button" value="Guardar">
				</div>
			</div>
		</form>
	</div>
</div>