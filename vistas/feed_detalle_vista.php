<!-- Ventana Modal Eliminar-->
<div class="row modal fade" id="modal_eliminar">
	<div
		class="modal-dialog modal-dialog-centered col-xs-12 modal_eliminar">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Eliminar Noticia</h4>
			</div>

			<div class="modal-body">
				<p>¿Deseas eliminar la noticia?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info botones" data-dismiss="modal">Cancelar</button>
				<a href="?a=eliminar&idFeed=<?php echo $feed->id; ?>" class="btn btn-info botones" name="eliminar" role="button">Aceptar</a>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<section class="col-md-12">
		<h2 class="page-header col-xs-12">Editar Noticias</h2>
	</section>
</div>

<div class="row">
	<div class="col-md-6 col-xs-6">
		<a class="btn boton_header" href="index.php" role="button">Listado Noticias</a>
	</div>
</div>
<div class="fotoEditar">
	<img src="<?php  echo $feed->image; ?>" alt="<?php  echo $feed->title; ?>">
</div>

<div class="row">
	<div class="col-md-12 col-xs-12">
		<form id="editar_noticia" name="editar_noticia" method="post" action="?a=actualizar&idFeed=<?php echo $feed->id; ?>" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12 col-xs-12 datos_noticia">
					<div class="col-md-3">
						<label class="datos_noticia">Foto</label> <input type="file" name="fotoNoticia" value="" />
					</div>
					<div class="col-md-3">
						<label class="datos_noticia">Title</label> <input class="campos" type="text" name="title" value="<?php  echo $feed->title; ?>" />
					</div>
					<div class="col-md-3">
						<label class="datos_noticia">Source</label> <input class="campos" type="text" name="source" value="<?php  echo $feed->source; ?>" />
					</div>
					<div class="col-md-3">
						<label class="datos_noticia">Publisher</label>
						<input class="campos" type="text" name="publisher" value="<?php  echo $feed->publisher; ?>" />
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 col-xs-12 campo">
					<label class="titulo_body">Body</label>
				</div>
			</div>
			<div class="row">
				<textarea name="body" class="descripcion" rows="8"><?php  echo $feed->body; ?></textarea>
				<br>
			</div>

			<div class="row">
				<div class="col-md-12 col-xs-12">
					<a class="btn btn-info botones" role="button" data-toggle="modal" data-target="#modal_eliminar">Eliminar</a>
					<input class="btn btn-info botones" type="submit" name="actualizar" value="Actualizar" role="button" />
				</div>
			</div>
		</form>
	</div>
</div>