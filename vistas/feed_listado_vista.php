

<div class="row">
	<section class="col-md-12">
		<h2 class="page-header col-xs-12">Listado de Noticias</h2>
	</section>
</div>
<div class="row">
	<div class="col-md-6 col-xs-6">
		<a class="btn boton_header" href="?a=nuevo" role="button">Crear Noticia</a>
	</div>
</div>
<div class="row">
	<?php 
// Se muestran las noticias
        foreach ($listado_feeds as $feed) {
    ?>
        	<div class="card col-md-6 noticias">
				<img class="card-img-top img_noticia" src="<?php echo $feed->image?>" alt="<?php echo $feed->title?>">
				<div class="card-body">
    				<h2 class="card-title title_noticia"><?php echo $feed->title?></h2>
    				<h6 class="card-subtitle mb-2 text-muted">Source: <?php echo $feed->source?></h6>
    				<h6 class="card-subtitle mb-2 text-muted"><?php echo $feed->publisher?></h6>
    				<p class="body_noticia"><?php echo nl2br($feed->body)?></p>
				</div>
				<div>
					<a href="?a=obtener&idFeed=<?php echo $feed->id; ?>" class="btn btn-primary boton_videojuegos">Editar Noticia</a>
				</div>
			</div>
	<?php
        }
    ?>
</div>
